﻿using System;
using Avalonia;
using Avalonia.Logging.Serilog;
using TabControlIssue.Shell.ViewModels;
using TabControlIssue.Shell.Views;

namespace TabControlIssue.Shell
{
    class Program
    {
        static void Main(string[] args)
        {
            BuildAvaloniaApp().Start<MainWindow>(() => new MainWindowViewModel());
        }

        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .UseReactiveUI()
                .LogToDebug();
    }
}

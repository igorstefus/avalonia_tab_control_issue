﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TabControlIssue.Shell.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public string Greeting => "Hello World!";
    }
}
